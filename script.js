// #Теоретический вопрос
//
// Опишите своими словами разницу между функциями setTimeout() и setInterval().
// setTimeout() вызывает функцию один раз через определённый интервал времени.
// setInterval() вызывает функцию регулярно, повторяя вызов через определённый интервал времени.

//     Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
//      функция не вызывается немедленно. Вместо этого он помещается в очередь, и сработает мгновенно, после завершения исполнения текущего скрипта. Эта функция делает события асинхронными.
//     Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?
//     Для того что бы отменить многократное повторение функции setInterval()
//
//
//     Задание
//     Реализовать программу, показывающую циклично разные картинки.
//
//     Технические требования:
//
//     В папке banners лежит HTML код и папка с картинками.
//     При запуске программы на экране должна отображаться первая картинка.
//     Через 10 секунд вместо нее должна быть показана вторая картинка.
//     Еще через 10 секунд - третья.
//     Еще через 10 секунд - четвертая.
//     После того, как покажутся все картинки - этот цикл должен начаться заново.
//     При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
//     По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
//     Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки,
//     которая в данный момент показана на экране.
//     Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
//
//
//     Необязательное задание продвинутой сложности:
//
//     При запуске программы на экране должен быть таймер с секундами и миллисекундами, показывающий сколько осталось до показа следующей картинки.
//     Делать скрытие картинки и показывание новой картинки постепенным (анимация fadeOut / fadeIn) в течение 0.5 секунды.


let imgIndex = 0;
let imgTimer;
let ms = 10000;
const timer = document.getElementById('timer');
const images = document.getElementsByTagName("img");

play();

function play() {
    if (imgTimer === undefined) {
        imgTimer = setInterval(function () {
            viewImages();
            countDown();
            ms -= 100;
            if (ms === 100) {
                ms = 10000;
            }
        }, 100);
    }
}

function stop() {
    clearInterval(imgTimer);
    imgTimer = undefined;
}

function viewImages() {
    if(ms === 10000) {
        for (let i = 0; i < images.length; i++) {
            images[i].style.opacity = '0';
        }

        images[imgIndex].style.opacity = '1';
        imgIndex++;

        if (imgIndex === images.length) {
            imgIndex = 0;
        }
    }
}

function countDown() {
    let res = Math.floor(ms / 1000);
    let milliseconds = ms.toString().substr(-3);
    let seconds = res % 60;

    document.getElementById("timer").innerHTML = seconds + ' s '+' : ' + milliseconds + ' ms';
}

//
// let imgTimer;
// let ms = 10000;
//
//
// play();
//
// function play() {
//     if (imgTimer === undefined) {
//         imgTimer = setInterval(function () {
//             if (ms === 10000) {
//                 rotate();
//             }
//             countDown();
//             ms -= 100;
//             if (ms === 100) {
//                 ms = 10000;
//             }
//         }, 100);
//     }
// }
//
// function stop() {
//     clearInterval(imgTimer);
//     imgTimer = undefined;
// }
//
//
// function countDown() {
//     let res = Math.floor(ms / 1000);
//     let milliseconds = ms.toString().substr(-3);
//     let seconds = res % 60;
//
//     document.getElementById("timer").innerHTML = seconds + ' s ' + milliseconds + ' ms';
// }
//
//
// // function theRotator() {
// //     $('div#imageBlock img').css({opacity: 0.0});
// //
// //     $('div#imageBlock img:first').css({opacity: 1.0});
// //
// //     setInterval('rotate()', 10000);
// // }
//
// function rotate() {
//     let current = ($('div#imageBlock img.show') ? $('div#imageBlock img.show') : $('div#imageBlock img:first'));
//
//     let next = ((current.next().length) ? ((current.next().hasClass('show')) ? $('div#imageBlock img:first')
//         : current.next()) : $('div#imageBlock img:first'));
//
//     next.css({opacity: 0.0})
//         .addClass('show')
//         .animate({opacity: 1.0}, 1000);
//
//     current.animate({opacity: 0.0}, 1000)
//         .removeClass('show');
// };
//
// // $(document).ready(function () {
// //     theRotator();
// // });